package main

import (
	"fmt"
	"qiscus-mqtt/qiscus"
)

func main() {
	// Change the app id with yours. In Mekari, you can get it from database
	appID := "appu-bxo0bgp6agfyrsof"

	// In Mekari Chat you may change this base url
	baseURL := "https://api3.qiscus.com"

	client := qiscus.NewSDKClient(appID, baseURL)
	config, _, err := client.GetAppConfig()
	if err != nil {
		fmt.Println(err)
		return
	}

	brokerConfig, err := qiscus.GetMQTTBrokerInfo(config.BrokerLbURL)
	if err != nil {
		fmt.Println(err)
		return
	}

	// can be "mqtt" or "mqtts"
	service, err := qiscus.NewMQTTService("mqtts", brokerConfig.URL, brokerConfig.SslPort)
	if err != nil {
		fmt.Println(err)
		return
	}

	// You can refer the topic from Ruby example
	topic := "12345678/c"

	// You can refer the payload from Ruby example
	payload := "Message payload"

	service.Publish(topic, payload)
}
