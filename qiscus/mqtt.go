package qiscus

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type MQTTService struct {
	Client mqtt.Client
}

func NewMQTTService(scheme string, host string, port string) (*MQTTService, error) {
	if scheme == "" {
		scheme = "mqtt"
	}

	if port == "" || scheme == "mqtt" {
		port = "1883"
	}

	server := fmt.Sprintf("%s://%s:%s", scheme, host, port)
	clientID := generateMQTTClientID()
	options := mqtt.NewClientOptions().
		AddBroker(server).
		SetClientID(clientID).
		SetCleanSession(true).
		SetAutoReconnect(true).
		SetKeepAlive(5 * time.Second).
		SetPingTimeout(5 * time.Second).
		SetConnectTimeout(5 * time.Second).
		SetMaxReconnectInterval(10 * time.Second)

	client := mqtt.NewClient(options)
	if token := client.Connect(); token.Wait() {
		err := token.Error()
		if err != nil {
			return nil, err
		}
	}

	return &MQTTService{
		Client: client,
	}, nil
}

func (service *MQTTService) Publish(topic string, payload string) {
	qos := 1
	retained := false

	if token := service.Client.Publish(topic, byte(qos), retained, payload); token.Wait() {
		err := token.Error()
		if err != nil {
			fmt.Println(err)
			if !service.Client.IsConnected() {
				service.Client.Connect()
				service.Client.Publish(topic, byte(qos), retained, payload)
			}
		}
	}
}

func generateMQTTClientID() string {
	return fmt.Sprintf("%s-%d", "com.qiscus.sdkmekarichat", time.Now().Unix())
}

type getBrokerInfoResp struct {
	BrokerConfig BrokerConfig `json:"data"`
	Node         string       `json:"node"`
	Status       int          `json:"status"`
}

type BrokerConfig struct {
	SslPort string `json:"ssl_port"`
	URL     string `json:"url"`
	WssPort string `json:"wss_port"`
}

func GetMQTTBrokerInfo(lbURL string) (*BrokerConfig, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", lbURL, nil)
	if err != nil {
		return nil, err
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var brokerInfoResp getBrokerInfoResp
	json.Unmarshal(body, &brokerInfoResp)

	return &brokerInfoResp.BrokerConfig, nil
}
