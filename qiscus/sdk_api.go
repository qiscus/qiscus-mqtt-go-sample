package qiscus

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type SDKClient struct {
	AppID      string
	BaseURL    string
	HTTPClient *http.Client
}

func NewSDKClient(appID string, baseURL string) *SDKClient {
	if baseURL == "" {
		baseURL = "https://api3.qiscus.com"
	}

	return &SDKClient{
		AppID:      appID,
		BaseURL:    baseURL,
		HTTPClient: &http.Client{},
	}
}

type getAppConfigResp struct {
	AppConfig AppConfig `json:"results"`
	Status    int       `json:"status"`
}

type AppConfig struct {
	AppCode             string `json:"app_code"`
	BaseURL             string `json:"base_url"`
	BrokerLbURL         string `json:"broker_lb_url"`
	BrokerURL           string `json:"broker_url"`
	EnableEventReport   bool   `json:"enable_event_report"`
	EnableRealtime      bool   `json:"enable_realtime"`
	EnableRealtimeCheck bool   `json:"enable_realtime_check"`
	Extras              string `json:"extras"`
	Status              string `json:"status"`
	SyncInterval        int    `json:"sync_interval"`
	SyncOnConnect       int    `json:"sync_on_connect"`
}

func (qiscusSDK *SDKClient) GetAppConfig() (*AppConfig, *http.Response, error) {
	url := fmt.Sprintf("%s/api/v2/sdk/config", qiscusSDK.BaseURL)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, nil, err
	}

	req.Header.Add("Qiscus-Sdk-App-Id", qiscusSDK.AppID)
	res, err := qiscusSDK.HTTPClient.Do(req)
	if err != nil {
		return nil, res, err
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return nil, res, err
	}

	var appConfigResp getAppConfigResp
	json.Unmarshal(body, &appConfigResp)

	return &appConfigResp.AppConfig, res, nil
}
